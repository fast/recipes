### A Pluto.jl notebook ###
# v0.19.40

using Markdown
using InteractiveUtils

# ╔═╡ 82e38aca-d718-11ee-0b23-ef277ca7fdae
begin
	import Pkg
	Pkg.activate("$(homedir())/Skola/VTI/FAST/FST/.julia_env")
	using Semirings
	using TensorFSTs
	using Zygote
	import SumSparseTensors: SumSparseTensor, SortedSumSparseTensor
end

# ╔═╡ 3330dbad-3418-4885-b870-5f747251ecd3
function project_to(T, Ā::NamedTuple{(:dims, :M, :α, :ω)}, A::TensorFST{S}) where S
    TensorFST{T}(
        dimensions(A),
        project_to(T, Ā.M, A.M),
        project_to(T, Ā.α, A.α),
        project_to(T, Ā.ω, A.ω)
    )
end

# ╔═╡ 2d59b7b8-3255-49b7-8a06-14e130889712
function project_to(
	T, ΣĀ::NamedTuple{(:axes, :nzcoo, :nzval)}, ΣA::SumSparseTensor{S,N,M}
) where {S,N,M}
    SumSparseTensor(
        ΣA.axes,
        ΣA.nzcoo,
        map(t -> T(t.val), ΣĀ.nzval)
    )
end

# ╔═╡ b0ffa8d7-7a27-4b0b-ba37-a9162b73ecfa
function project_to(
	T, ΣĀ::NamedTuple{(:axes, :ptr, :nzcoo, :nzval)}, ΣA::SortedSumSparseTensor{S,N,I,M}
) where {S,N,I,M}
    SortedSumSparseTensor{T,N,I}(
        ΣA.axes,
        ΣA.ptr,
        ΣA.nzcoo,
        map(t -> T(t.val), ΣĀ.nzval)
    )
end

# ╔═╡ 80efdee1-7de2-4ab1-b090-0f8385ca603f
S = ArcticSemiring{Float64}

# ╔═╡ 3abc0604-bfec-4394-84ec-6fe3b6c0bcca
A = TensorFST(
    [
        (src = (1,), isym = 2, osym = 2, weight = S(0.5), dest = (2,)),
        (src = (1,), isym = 3, osym = 2, weight = S(0.2), dest = (2,)),
        (src = (2,), isym = 3, osym = 3, weight = S(1.0), dest = (4,)),
        (src = (1,), isym = 2, osym = 3, weight = S(0.3), dest = (3,)),
        (src = (3,), isym = 3, osym = 3, weight = S(1.0), dest = (4,))
    ],
    [(1,) => S(1)],
    [(4,) => S(1)]
)

# ╔═╡ bd702a5c-f039-46e8-9fde-f72570f16fdc
W, (Ā,) = withgradient(val∘weight, A)

# ╔═╡ 560ef89b-4bee-4e9b-96c4-5c9038bb44b5
prĀ = project_to(ProbSemiring{Float64}, Ā, A)

# ╔═╡ 9a0b177e-b107-4f29-922b-5626970b937c
draw(A)

# ╔═╡ aa93334f-c0e2-4cff-ae0e-be589dac14ef
draw(prĀ)

# ╔═╡ Cell order:
# ╠═82e38aca-d718-11ee-0b23-ef277ca7fdae
# ╠═3330dbad-3418-4885-b870-5f747251ecd3
# ╠═2d59b7b8-3255-49b7-8a06-14e130889712
# ╠═b0ffa8d7-7a27-4b0b-ba37-a9162b73ecfa
# ╠═80efdee1-7de2-4ab1-b090-0f8385ca603f
# ╠═3abc0604-bfec-4394-84ec-6fe3b6c0bcca
# ╠═bd702a5c-f039-46e8-9fde-f72570f16fdc
# ╠═560ef89b-4bee-4e9b-96c4-5c9038bb44b5
# ╠═9a0b177e-b107-4f29-922b-5626970b937c
# ╠═aa93334f-c0e2-4cff-ae0e-be589dac14ef
